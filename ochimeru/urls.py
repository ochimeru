from django.conf.urls.defaults import *
import os

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^ochimeru/', include('ochimeru.foo.urls')),
    (r'^$',                                             'ochimeru.cards.views.home'),
    (r'^login$',                                        'ochimeru.cards.views.login_user'),
    (r'^level/(?P<level>\d+)/$',                        'ochimeru.cards.views.level'),
    (r'^level/(?P<level>\d+)/(?P<card_id>\d+)$',        'ochimeru.cards.views.card'),
    (r'^level/(?P<level>\d+)/(?P<card_id>\d+)/show$',   'ochimeru.cards.views.answer'),
    (r'^level/(?P<level>\d+)/(?P<card_id>\d+)/check$',  'ochimeru.cards.views.check'),
    (r'^browse/(?P<page>\d*)',                          'ochimeru.cards.views.browse'),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.abspath('%s/static/' % os.path.dirname(__file__))}),
)

print  os.path.abspath('%s/static/' % os.path.dirname(__file__))

