from django.db import models
from django.contrib.auth.models import User

class Card(models.Model):
    question = models.CharField(max_length=100, unique=True)
    answer = models.CharField(max_length=100)

    def __unicode__(self):
        return self.question

class CardStatus(models.Model):
    card = models.ForeignKey(Card)
    user = models.ForeignKey(User)
    level = models.PositiveIntegerField()
    is_current = models.BooleanField()


