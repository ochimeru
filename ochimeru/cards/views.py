from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, Http404, HttpResponseForbidden
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from ochimeru.cards.models import Card, CardStatus

import random

WORKING_SET_SIZE = 50

def home(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login')

    cards = Card.objects.count()

    levels = [x['level'] for x in CardStatus.objects.filter(user=request.user).values('level').distinct()]
    if 0 not in levels: levels.append(0)

    levels = range(min(levels), max(levels)+1)

    level_counts = []
    sum = 0

    for l in levels:
        if l == 0: continue
        count = CardStatus.objects.filter(user=request.user, level=l).count()
        level_counts.append({'level': l, 'count': count})
        sum += count

    level_counts.append({'level': 0, 'count': cards - sum})

    level_counts.sort(key = lambda x: x['level'], reverse=True)
    
    return render_to_response('level.html', {
        'levels': level_counts})


@login_required
def level(request, level):
    set = [x.card for x in CardStatus.objects.filter(user=request.user, level=level, is_current=True)]

    for c in Card.objects.order_by('?'):

        if len(set) >= WORKING_SET_SIZE: break

        status = CardStatus.objects.filter(user=request.user, card=c)

        if status and status[0].level == level:
            status[0].is_current = True
            status[0].save()
            set.append(c)

        elif not status and int(level) == 0:
            CardStatus.objects.create(user=request.user, card=c, is_current=True, level=0)
            set.append(c)

    if set:
        card = random.choice(set)
        return HttpResponseRedirect('/level/%s/%d' % (level, card.id))
    else:
        return HttpResponseRedirect('/')


@login_required
def card(request, level, card_id):
    card = Card.objects.get(pk=card_id)

    return render_to_response('card.html', {
        'level': level,
        'card': card
        })


@login_required
def answer(request, level, card_id):
    card = Card.objects.get(pk=card_id)

    return render_to_response('answer.html', {
        'level': level,
        'card': card
        })

def check(request, level, card_id):
    card = Card.objects.get(pk=card_id)
    status = CardStatus.objects.get(card=card, user=request.user)

    if 'correct' in request.GET:
        status.level += 1

    else:
        status.level = 0

    status.save()

    return HttpResponseRedirect('/level/%s/' % level)


@login_required
def browse(request, page):
    cards = Card.objects.all()
    paginator = Paginator(cards, 25)

    try:
        page = int(page)
    except ValueError:
        page = 1


    try:
        page_cards = paginator.page(page)
    except (EmptyPage, InvalidPage):
        page_cards = paginator.page(paginator.num_pages)


    return render_to_response('browse.html', {'cards': page_cards})



def login_user(request):
    try:
        username = request.POST['user']
        password = request.POST['pwd']
    except:
        return render_to_response('login.html')

    user = authenticate(username=username, password=password)

    if not user:
        return render_to_response('login.html', {
            'error_message': _('Unknown user or wrong password'),
            })

    login(request, user)

    return HttpResponseRedirect('/')

